from django.urls import path

from core import views

app_name = "core"


urlpatterns = [
    ##### GRANTGOAL URLS #####
    path('list/grantgoal/', views.ListGrantGoal.as_view(), name="list_gg"),
    path('new/grantgoal/', views.NewGrantGoal.as_view(), name="create_gg"),
    path('detail/grantgoal/<int:pk>/', views.DetailGrantGoal.as_view(), name="detail_gg"),
    path('update/grantgoal/<int:pk>/', views.UpdateGrantGoal.as_view(), name="update_gg"),
    path('delete/grantgoal/<int:pk>/', views.DeleteGrantGoal.as_view(), name="delete_gg"),

    ##### SUBGRANTGOAL URLS #####
    path('list/subgrantgoal/', views.ListSubGrantGoal.as_view(), name="list_sgg"),
    path('new/subgrantgoal/', views.NewSubGrantGoal.as_view(), name="create_sgg"),
    path('detail/subgrantgoal/<int:pk>/', views.DetailSubGrantGoal.as_view(), name="detail_sgg"),
    path('update/subgrantgoal/<int:pk>/', views.UpdateSubGrantGoal.as_view(), name="update_sgg"),
    path('delete/subgrantgoal/<int:pk>/', views.DeleteSubGrantGoal.as_view(), name="delete_sgg"),


    ### Issues ####
    path('list/issue/', views.ListIssue.as_view(), name="list_issue"),
    path('new/issue/', views.NewIssue.as_view(), name="create_issue"),
    path('detail/issue/<int:pk>/', views.DetailIssue.as_view(), name="detail_issue"),
    path('update/issue/<int:pk>/', views.UpdateIssue.as_view(), name="update_issue"),
    path('delete/issue/<int:pk>/', views.DeleteIssue.as_view(), name="delete_issue"),


    ### AREA ####
    path('new/area/', views.NewArea.as_view(), name="create_area"),
    path('list/area/', views.ListArea.as_view(), name="list_area"),
    path('detail/area/<int:pk>/', views.DetailArea.as_view(), name="detail_area"),
    path('update/area/<int:pk>/', views.UpdateArea.as_view(), name="update_area"),
    path('delete/area/<int:pk>/', views.DeleteArea.as_view(), name="delete_area"),


    ### GOAL ###
    path('goal/new/', views.NewGoal.as_view(), name='create_goal'),
    path('goal/', views.ListGoal.as_view(), name='list_goal'),
    path('goal/<int:pk>/', views.DetailGoal.as_view(), name='detail_goal'),
    path('goal/update/<int:pk>/', views.UpdateGoal.as_view(), name='update_goal'),
    path('goal/delete/<int:pk>/', views.DeleteGoal.as_view(), name='delete_goal'),

]