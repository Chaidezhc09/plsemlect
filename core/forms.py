from django import forms
from .models import GrantGoal, SubGrantGoal, Issue, Area, Goal

#GrantGoal
class NewGrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = [
            "ggname",
            "description",
            "user",
            "days_duration",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "ggname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Name"}),
            "description": forms.Textarea(attrs={"rows": 3, "type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Description"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Slug"}),
        }



class UpdateGrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = [
            "ggname",
            "description",
            "user",
            "days_duration",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "ggname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Name"}),
            "description": forms.Textarea(attrs={"rows": 3, "type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Description"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Slug"}),
        }

    
# Sub GrantGoal
class NewSubGrantGoalForm(forms.ModelForm):
    class Meta:
        model = SubGrantGoal
        fields = [
            "sggname",
            "description",
            "user",
            "grantgoal",
            "area",
            "days_duration",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "sggname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Name"}),
            "description": forms.Textarea(attrs={"rows": 3, "type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Description"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "grantgoal": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "area": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Slug"}),
        }



class UpdateSubGrantGoalForm(forms.ModelForm):
    class Meta:
        model = SubGrantGoal
        fields = [
            "sggname",
            "description",
            "user",
            "grantgoal",
            "area",
            "days_duration",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "sggname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Name"}),
            "description": forms.Textarea(attrs={"rows": 3, "type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Description"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "grantgoal": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "area": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el GrantGoal Slug"}),
        }




# Issues
class NewIssueForm(forms.ModelForm):
    class Meta:
        model = Issue
        fields = [
            "issuename",
            "description",
            "goal",
            "area",
            "user",
            # "timestamp",
            "days_duration",
            "priority",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "issuename": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter Issue Name"}),
            "description": forms.Textarea(attrs={"rows": 3, "type":"text", "class":"form-control", "placeholder":"Enter Issue Description"}),
            "goal": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "area": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "timestamp": forms.DateInput(attrs={"type":"date", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "priority": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter Issue Slug"}),
        }
    

class UpdateIssueForm(forms.ModelForm):
    class Meta:
        model = Issue
        fields = [
            "issuename",
            "description",
            "goal",
            "area",
            "user",
            # "timestamp",
            "days_duration",
            "priority",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "issuename": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter Issue Name"}),
            "description": forms.Textarea(attrs={"rows": 3, "type":"text", "class":"form-control", "placeholder":"Enter Issue Description"}),
            "goal": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "area": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "timestamp": forms.DateInput(attrs={"type":"date", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "priority": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter Issue Slug"}),
        }


        # AREA
from django import forms
from .models import Area

class NewAreaForm(forms.ModelForm):
    class Meta:
        model = Area
        fields = [
            "area_name",
            "description"
        ]
        widgets = {
            "area_name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del área"}),
            "description": forms.Textarea(attrs={"rows": 3, "type":"text", "class":"form-control", "placeholder":"Escribe la descripción del área"}),
        }

class UpdateAreaForm(forms.ModelForm):
    class Meta:
        model = Area
        fields = [
            "area_name",
            "description"
        ]
        widgets = {
            "area_name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del área"}),
            "description": forms.Textarea(attrs={"rows": 3, "type":"text", "class":"form-control", "placeholder":"Escribe la descripción del área"}),
        }



#Goal
class NewGoalForm(forms.ModelForm):
    class Meta:
        model = Goal
        fields = [
            "goalname",
            "description",
            "subgrantgoal",
            "area",
            "user",
            "days_duration",
            "priority",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "goalname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter Goal Name"}),
            "description": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter Goal Description"}),
            "subgrantgoal": forms.Select(attrs={"class":"form-control"}),
            "area": forms.Select(attrs={"class":"form-control"}),
            "user": forms.Select(attrs={"class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "priority": forms.Select(attrs={"class":"form-control"}),
            "state": forms.Select(attrs={"class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter Goal Slug"}),
        }

class UpdateGoalForm(forms.ModelForm):
    class Meta:
        model = Goal
        fields = [
            "goalname",
            "description",
            "subgrantgoal",
            "area",
            "user",
            "days_duration",
            "priority",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "goalname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter Goal Name"}),
            "description": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter Goal Description"}),
            "subgrantgoal": forms.Select(attrs={"class":"form-control"}),
            "area": forms.Select(attrs={"class":"form-control"}),
            "user": forms.Select(attrs={"class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "priority": forms.Select(attrs={"class":"form-control"}),
            "state": forms.Select(attrs={"class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Enter Goal Slug"}),
        }

