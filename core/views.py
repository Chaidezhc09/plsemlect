from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy

from core import models
from core import forms 

# Create your views here.

# # # #  C R U D G R A N T G O A L  # # # # #

### CREATE
class NewGrantGoal(generic.CreateView):
    template_name = "core/create_gg.html"
    model = models.GrantGoal
    form_class = forms.NewGrantGoalForm
    success_url = reverse_lazy("core:list_gg")

## List
class ListGrantGoal(generic.View):
    template_name = "core/list_gg.html"
    context = {}

    def get(self, request):
        self.context = {
            "grantgoals": models.GrantGoal.objects.filter(status=True)
        }
        return render(request, self.template_name, self.context)
    
## Detail
class DetailGrantGoal(generic.View):
    template_name = "core/detail_gg.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "grantgoal": models.GrantGoal.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)

### UPDATE
class UpdateGrantGoal(generic.UpdateView):
    template_name = "core/update_gg.html"
    model = models.GrantGoal
    form_class = forms.UpdateGrantGoalForm
    success_url = reverse_lazy("core:list_gg")


### DELETE
class DeleteGrantGoal(generic.DeleteView):
    template_name = "core/delete_gg.html"
    model = models.GrantGoal
    success_url = reverse_lazy("core:list_gg")



# # # #  C R U D   S U B G R A N T G O A L  # # # # #

### CREATE
class NewSubGrantGoal(generic.CreateView):
    template_name = "core/create_sgg.html"
    model = models.SubGrantGoal
    form_class = forms.NewSubGrantGoalForm
    success_url = reverse_lazy("core:list_sgg")

### RETRIEVE
## List
class ListSubGrantGoal(generic.ListView):
    template_name = "core/list_sgg.html"
    queryset = models.SubGrantGoal.objects.filter(status=True)

## Detail
class DetailSubGrantGoal(generic.DetailView):
    template_name = "core/detail_sgg.html"
    model = models.SubGrantGoal


### UPDATE
class UpdateSubGrantGoal(generic.UpdateView):
    template_name = "core/update_sgg.html"
    model = models.SubGrantGoal
    form_class = forms.UpdateSubGrantGoalForm
    success_url = reverse_lazy("core:list_sgg")


### DELETE
class DeleteSubGrantGoal(generic.DeleteView):
    template_name = "core/delete_sgg.html"
    model = models.SubGrantGoal
    success_url = reverse_lazy("core:list_sgg")



# # # #  C R U D G O A L  # # # # #

### CREATE
class NewGoal(generic.CreateView):
    template_name = "core/create_goal.html"
    model = models.Goal
    form_class = forms.NewGoalForm
    success_url = reverse_lazy("core:list_goal")

## List
class ListGoal(generic.View):
    template_name = "core/list_goal.html"
    context = {}

    def get(self, request):
        self.context = {
            "goals": models.Goal.objects.filter(status=True)
        }
        return render(request, self.template_name, self.context)
    
## Detail
class DetailGoal(generic.View):
    template_name = "core/detail_goal.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "goal": models.Goal.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)

### UPDATE
class UpdateGoal(generic.UpdateView):
    template_name = "core/update_goal.html"
    model = models.Goal
    form_class = forms.UpdateGoalForm
    success_url = reverse_lazy("core:list_goal")


### DELETE
class DeleteGoal(generic.DeleteView):
    template_name = "core/delete_goal.html"
    model = models.Goal
    success_url = reverse_lazy("core:list_goal")



# # # #  C R U D   ISSUES  # # # # #

### CREATE
class NewIssue(generic.CreateView):
    template_name = "core/create_issue.html"
    model = models.Issue
    form_class = forms.NewIssueForm
    success_url = reverse_lazy("core:list_issue")


## List

class ListIssue(generic.View):
    template_name = "core/list_issue.html"
    context = {}

    def get(self, request):
        self.context = {
            "issues": models.Issue.objects.filter(status=True)
        }
        return render(request, self.template_name, self.context)
    

## Detail
class DetailIssue(generic.View):
    template_name = "core/detail_issue.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "issue": models.Issue.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)


### UPDATE
class UpdateIssue(generic.UpdateView):
    template_name = "core/update_issue.html"
    model = models.Issue
    form_class = forms.UpdateIssueForm
    success_url = reverse_lazy("core:list_issue")


### DELETE
class DeleteIssue(generic.DeleteView):
    template_name = "core/delete_issue.html"
    model = models.Issue
    success_url = reverse_lazy("core:list_issue")








# # # #  C R U D   AREA  # # # # #

# CREATE
class NewArea(generic.CreateView):
    template_name = "core/create_area.html"
    model = models.Area
    form_class = forms.NewAreaForm
    success_url = reverse_lazy("core:list_area")

# LIST
class ListArea(generic.View):
    template_name = "core/list_area.html"
    context = {}

    def get(self, request):
        self.context = {
            "areas": models.Area.objects.all()
        }
        return render(request, self.template_name, self.context)

# DETAIL
class DetailArea(generic.View):
    template_name = "core/detail_area.html"
    context = {}

    def get(self, request, pk):
        self.context = {
            "area": models.Area.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)

# UPDATE
class UpdateArea(generic.UpdateView):
    template_name = "core/update_area.html"
    model = models.Area
    form_class = forms.UpdateAreaForm
    success_url = reverse_lazy("core:list_area")

# DELETE
class DeleteArea(generic.DeleteView):
    template_name = "core/delete_area.html"
    model = models.Area
    success_url = reverse_lazy("core:list_area")