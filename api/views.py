from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import generics

from .serializer import ListGrantGoalSerializer, DetailGrantGoalSerializer, CreateGrantGoalSerializer, UpdateGrantGoalSerializer
from core.models import GrantGoal


class CreateGrantGoalAPIView(generics.CreateAPIView):
    serializer_class = CreateGrantGoalSerializer
    



class ListGrantGoalAPIView(APIView):

    def get(self, request):
        queryset = GrantGoal.objects.all()
        data = ListGrantGoalSerializer(queryset, many=True).data
        return Response(data)
    

class DetailGrantGoalAPIView(APIView):

    def get(self, request, pk):
        queryset = GrantGoal.objects.get(pk=pk)
        data = DetailGrantGoalSerializer(queryset, many=False).data
        return Response(data)
    
class UpdateGrantGoalAPIView(generics.UpdateAPIView):
    serializer_class = UpdateGrantGoalSerializer
    queryset= GrantGoal.objects.all()
    
class DeleteGrantGoalAPIView(generics.DestroyAPIView):
    serializer_class = DetailGrantGoalSerializer
    queryset= GrantGoal.objects.all()